/*
*
* Authors: Fernando Amat
*  test_TGMM_parser_XML.cpp
*
*  Created on : March 22nd, 2015
* Author : Fernando Amat
*
* \brief Testing suite for XML importer from TGMM output to CSV
* 
*/

#include <string>
#include "TGMMparserXML.h"



int main(int argc, const char** argv)
{
	std::cout << "test TGMM parser XML running..." << std::endl;

	//inputs
	//TODO: autodetect data folder
	std::string filename ("C:/Users/Fernando/cppProjects/TGMMoutputParser/test/data/GMEMfinalResult_frame0015.xml");
	
	if (argc > 1)
		filename = std::string(argv[1]);

	//initialize classes
	TGMM_parse_XML parser;
	IOclass_local_file fileInfo(filename);

	//setup info
	parser.set_IO_path(fileInfo);

	//read XML file
	int err = parser.read_from_TGMM_output();
	if (err != 0)
		return err;

	//write output as CSV file
	std::string filenameCSV(filename + ".csv");
	err = parser.write_to_xpiwit_CSV(filenameCSV);
	if (err != 0)
		return err;

	//make copy
	TGMM_parse_XML parserA (parser);

	//read CSV file	
	err = parser.read_from_xpiwit_CSV(filenameCSV);
	if (err != 0)
		return err;

	//check that both parsers contain the same information
	if (parser != parserA)
	{
		std::cout << "ERROR: parsers are not equal" << std::endl;
		return 10;
	}

	std::cout << "test TGMM parser XML passed" << std::endl;
	return 0;
}