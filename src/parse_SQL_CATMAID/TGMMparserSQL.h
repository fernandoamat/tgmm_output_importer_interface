/*
* Copyright (C) 2015 by  Fernando Amat
* See license.txt for full license and copyright notice.
*
* Authors: Fernando Amat
*  TGMMparserSQL.h
*
*  Created on: March 22nd, 2015
*      Author: Fernando Amat
*
* \brief Implementing interface to import tracking output from catmaid
*/

#ifndef __TGMM_PARSE_SQL_H__
#define __TGMM_PARSE_SQL_H__

#include <string>
#include <cstdint>
#include <cstring>
#include "PostgreSQL.h"
#include "../TGMMparserInterface.h"



//=================================================================================================
struct IOclass_SQL_database
{
	std::string server;
	std::string user;
	std::string password;
	std::string database;
	std::string projectName;

	IOclass_SQL_database() {};
};

//==============================================
struct CATMAID_stack_parameters
{
	std::int64_t projectId;
	std::int64_t stackId; 
	std::int64_t stackSize[3]; 
	float stackRes[3];
};

//=================================================================================================

class TGMM_parse_SQL :public TGMM_parse_interface<IOclass_SQL_database>
{
public:	
	
	//I/O method. If you don't specify TM it will retrieve all the time points
	int read_from_TGMM_output(int TM = -1);//virtual method that needs to be implemented for every possible type of access

	int read_DBinfo_from_txt_file(const std::string& filename, const std::string& projectName);//txt filename containing all the information about the PostgreSQL to be able to connect

	int connectToDatabase();
	int disconnectFromDatabase();
	int retrieveProjectInformation();

protected:
	
private:	
	template<class T>
	void parseValue3D(const std::string& valS, T valI[3]);//special type in CATMAID database

	CATMAID_stack_parameters stackParam;//contains stack information retrieved for a specific project

	DataBase<PostgreSql> dataBase;//main object that holds the connection to the database
};


//==============================================
template<class T>
void TGMM_parse_SQL::parseValue3D(const std::string& valS, T valI[3])
{
	//remove parenthesis
	std::string valSC = valS.substr(1, valS.length() - 2);


	double val[3];
	char* token[3] = {}; // initialize to 0
	// parse the line
	token[0] = strtok((char*)(valSC.c_str()), ","); // first token
	if (token[0]) // zero if line is blank
	{
		val[0] = atof(token[0]);
		for (int n = 1; n < 3; n++)
		{
			token[n] = strtok(0, ","); // subsequent tokens
			if (!token[n])
				break; // no more tokens
			val[n] = atof(token[n]);
		}
	}

	for (int ii = 0; ii < 3; ii++)
		valI[ii] = (T)(val[ii]);
};


#endif //end of __TGMM_PARSE_SQL_H__