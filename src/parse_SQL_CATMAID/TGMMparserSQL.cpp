/*
* Copyright (C) 2015 by  Fernando Amat
* See license.txt for full license and copyright notice.
*
* Authors: Fernando Amat
*  TGMMparseXML.cpp
*
*  Created on: March 22nd, 2015
*      Author: Fernando Amat
*
* \brief Implementing interface to import TGMM output from local XML files
*/

#include <assert.h>
#include <string>
#include <sstream>
#include <cstdlib>
#include "TGMMparserSQL.h"


#ifdef _MSC_VER
  #define atoll(x) _atoi64(x)
#endif

//=========================================================
int TGMM_parse_SQL::retrieveProjectInformation()
{
	//connect to database
	try{
		//connect to database				
		ResultSet rs;
		std::string value, sqlQuery;

		//obtain project id
		sqlQuery = std::string("SELECT id FROM project WHERE title = '" + fileInfo.projectName + "'");
		dataBase << sqlQuery, rs;

		if (rs.countRows() == 0)
		{
			throw DataBaseError("Project name not found in the database");
		}

		rs.fetch(0, value);
		stackParam.projectId = atoi(value.c_str());
		std::string projectIdStr(value);

		//obtain stack id associated with this project
		sqlQuery = std::string("SELECT stack_id FROM project_stack WHERE project_id = '" + projectIdStr + "'");

		dataBase << sqlQuery, rs;
		if (rs.countRows() == 0)
		{
			throw DataBaseError("Stack id for the project not found in the database");
		}

		rs.fetch(0, value);
		stackParam.stackId = atoi(value.c_str());
		std::string stackIdStr(value);

		//obtain information from stack
		sqlQuery = std::string("SELECT dimension, resolution FROM stack WHERE id = '" + stackIdStr + "'");
		rs.reset();
		dataBase << sqlQuery, rs;
		if (rs.countRows() == 0)
		{
			throw DataBaseError("Stack information for the project not found in the database");
		}
		//stack dimension
		value = rs.get(0, 0);
		parseValue3D<std::int64_t>(value, stackParam.stackSize);
		value = rs.get(0, 1);
		//stack resolution		
		parseValue3D<float>(value, stackParam.stackRes);

	}
	catch (const DataBaseError& e)
	{
		std::cout << e.what() << std::endl;
		return 2;
	}

	return 0;
}


//===========================================================
int TGMM_parse_SQL::connectToDatabase()
{
	//check if information has been setup
	if (!isSet_fileInfo)
	{
		std::cout << "ERROR: TGMM_parse_SQL::connectToDatabase: file info was not set before calling this function" << std::endl;
		return 1;
	}

	//connect to database
	try{
		//connect to database		
		dataBase.connect(fileInfo.server, fileInfo.user, fileInfo.password, fileInfo.database);			
	}
	catch (const DataBaseError& e)
	{
		std::cout << e.what() << std::endl;
		return 2;
	}

	return 0;
}

//===========================================================
int TGMM_parse_SQL::disconnectFromDatabase()
{

	//disconnect to database
	try{
		//connect to database		
		dataBase.close();
	}
	catch (const DataBaseError& e)
	{
		std::cout << e.what() << std::endl;
		return 2;
	}

	return 0;
}

//===============================================

int TGMM_parse_SQL::read_from_TGMM_output(int TM)
{
	
	int err = connectToDatabase();
	if (err != 0)
		return err;

	err = retrieveProjectInformation();
	if (err != 0)
		return err;

	//retrieve x,y,z points for the specified time point
	try{		
		ResultSet rs;
		std::string value, sqlQuery;

		std::ostringstream os, osTM;
		os << stackParam.projectId;
		osTM << TM;
		if ( TM >= 0)
			sqlQuery = std::string("SELECT id, location FROM treenode WHERE location_t=" + osTM.str() + " AND project_id = '" + os.str() + "' ORDER BY id");
		else
			sqlQuery = std::string("SELECT id, location FROM treenode WHERE project_id = '" + os.str() + "' ORDER BY id");
		
		dataBase << sqlQuery, rs;

		size_t numPts = rs.countRows();		

		if (numPts == 0)
		{
			std::cout << "WARNING: read_from_TGMM_output: points retrieved from the database" << std::endl;
			return 0;
		}

		obj_vec.resize(numPts); 
		for (size_t ii = 0; ii < numPts; ii++)
		{
			//parse information about this node
			value = rs.get(ii, 0);
			obj_vec[ii].obj_id = atoll(value.c_str());
			value = rs.get(ii, 1);
			parseValue3D<float>(value, obj_vec[ii].xyz);

			//parse to image coordinates
			for (int aa = 0; aa < 3; aa++)
				obj_vec[ii].xyz[aa] /= stackParam.stackRes[aa];

			obj_vec[ii].vol_id = -1;
		}		
	}
	catch (const DataBaseError& e)
	{
		std::cout << e.what() << std::endl;
		return 2;
	}

	//close database connection
	disconnectFromDatabase();
	

	return 0;
}

//==========================================================================

int TGMM_parse_SQL::read_DBinfo_from_txt_file(const std::string& filename, const std::string& projectName)
{
	std::ifstream fid(filename.c_str());

	if (!fid.is_open())
	{
		std::cerr << "ERROR::TGMM_parse_SQL::read_DBinfo_from_txt_file: file " << filename << " could not be opened to read DB informartion" << std::endl;
		return 3;
	}


	
	std::getline(fid,fileInfo.user);
	std::getline(fid,fileInfo.password);
	std::getline(fid,fileInfo.database);
	std::getline(fid,fileInfo.server);

	fid.close();

	fileInfo.projectName = projectName;

	isSet_fileInfo = true;

	return 0;
}